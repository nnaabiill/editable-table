      document.addEventListener("DOMContentLoaded", setData);
      const editBtn = document.getElementsByClassName("bEdit");
      const deletetBtn = document.getElementsByClassName("bDelete");
      const confirmBtn = document.getElementsByClassName("bConfirm");
      const cancelBtn = document.getElementsByClassName("bCancel");
      const mainTable = document.getElementById("tableBody");

      let mainData = [
        {
          id: 1,
          firstname: "firstname1",
          lastname: "lastname1",
          fullname: "fullname1",
          address: "address1",
        },
        {
          id: 2,
          firstname: "firstname2",
          lastname: "lastname2",
          fullname: "fullname2",
          address: "address2",
        },
        {
          id: 3,
          firstname: "firstname3",
          lastname: "lastname3",
          fullname: "fullname3",
          address: "address3",
        },
        {
          id: 4,
          firstname: "firstname4",
          lastname: "lastname4",
          fullname: "fullname4",
          address: "address4",
        },
        {
          id: 5,
          firstname: "firstname5",
          lastname: "lastname5",
          fullname: "fullname5",
          address: "address5",
        },
        
      ];

      function buildRow(data, rowSeiralNumber) {
        const html = `
                        <tr id="${data.id}">
                        <th scope="row">${rowSeiralNumber}</th>
                        <td col_name="firstname">${data.firstname}</td>
                        <td col_name="lastnaame">${data.lastname}</td>
                        <td col_name="fullname">${data.fullname}</td>
                        <td col_name="address">${data.address}</td>
                        <td name="bstable-actions" class="d-flex bsTable">
                            <button class="bEdit" data-row="${data.id}" id="bEdit${data.id}" type="button" class="btn btn-sm btn-default" onclick="edit(event);">
                                <span data-row="${data.id}" class="fa fa-edit"></span>
                            </button>
                            <button class="bDelete" data-row="${data.id}" id="bDelete${data.id}" type="button" data-toggle="modal" data-target="#exampleModal${data.id}" class="btn btn-sm btn-default">
                                <span data-row="${data.id}" class="fa fa-trash"></span>
                            </button>
                            <button class="bConfirm" data-row="${data.id}" id="bConfirm${data.id}" type="button" class="btn btn-sm btn-default" onclick="confirm(event);" style="display: none;">
                                <span data-row="${data.id}" class="fa fa-check-circle"></span>
                            </button>
                            <button class="bCancel" data-row="${data.id}" id="bCancel${data.id}" type="button" class="btn btn-sm btn-default" onclick="cancel(event);" style="display: none;">
                                <span data-row="${data.id}" class="fa fa-times-circle"></span>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal${data.id}" tabindex="-1" aria-labelledby="exampleModal${data.id}Label" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header text-white d-flext justify-content-center" style="background-color: #F93154;">
                                    <h5 class="modal-title" id="exampleModal${data.id}Label">Are you sure ?</h5>
                                  </div>
                                  <div class="modal-body">
                                    <div class="row text-center d-flex flex-column align-items-center">
                                        <i class="fa fa-trash" style="font-size: 50px; color: #F93154;"></i>
                                        
                                      </div>
                                  </div>
                                  <div class="modal-footer d-flex justify-content-center my-2">
                                    <button type="button" class="btn btn-outline-danger px-4" data-row="${data.id}" data-dismiss="modal" onclick="setChoice(event, false);">No</button>
                                    <button type="button" class="btn text-white px-4" style="background-color: #F93154;" data-row="${data.id}" data-dismiss="modal" onclick="setChoice(event, true);">Yes</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </td>
                        </tr>
                    `;
        return html;
      }

      function addNewRow(event) {
        const numberofRows = document.getElementsByTagName('tr');
        const id = `_${numberofRows.length + 1}`;
        let newRow = "";
        const data = {
          firstname: "",
          lastname: "",
          fullname: "",
          address: "",
        };

        let tr = document.createElement('tr');
        let th = document.createElement('th');
        tr.setAttribute('id', id);
        th.setAttribute('scope', 'row');
        th.textContent = '';
        let td = document.createElement('td');
        td.setAttribute('name', "bstable-actions");
        td.setAttribute('class', 'd-flex bsTable');

        // Create Add Button
        let addButton = document.createElement('button');
        addButton.setAttribute('class', 'bConfirm');
        addButton.setAttribute('data-row', id);
        addButton.setAttribute('id', `bConfirm${id}`);
        addButton.setAttribute('type', 'button');
        addButton.setAttribute('onclick', 'add(event)');
        let addSpan = document.createElement('span');
        addSpan.setAttribute('data-row', id);
        addSpan.setAttribute('class', 'fa fa-check-circle');
        addButton.appendChild(addSpan);
        
        // Create Cancel Button
        let cancelButton = document.createElement('button');
        cancelButton.setAttribute('class', 'bCancel');
        cancelButton.setAttribute('data-row', id);
        cancelButton.setAttribute('id', `bCancel${id}`);
        cancelButton.setAttribute('type', 'button');
        cancelButton.setAttribute('onclick', 'add(event)');
        let cancelSpan = document.createElement('span');
        cancelSpan.setAttribute('data-row', id);
        cancelSpan.setAttribute('class', 'fa fa-times-circle');
        cancelButton.appendChild(cancelSpan);

        td.appendChild(addButton);
        td.appendChild(cancelButton);
        
        tr.appendChild(th);
        tr.appendChild(td);
        // mainTable.firstElementChild.insertAdjacentElement('beforebegin', tr);
        mainTable.appendChild(tr);
        let row = document.getElementById(id);
        let values = {...data}
        for(let value in values) {
            let td = generateEditableRow(values, value);
            row.lastElementChild.insertAdjacentElement("beforebegin", td);
        }
      }

      function setData() {
        mainTable.innerHTML = "";
        let i = 1;
        for (let data of mainData) {
          mainTable.innerHTML += buildRow(data, i);
          i += 1;
        }
      }

      function setDefaultTable(values, row) {
        for (let value in values) {
          let td = document.createElement("td");
          td.setAttribute("col_name", value);
          td.textContent = values[value];
          row.lastElementChild.insertAdjacentElement("beforebegin", td);
        }
      }

      function generateEditableRow(values, value) {
        let td = document.createElement("td");
        let input = document.createElement("input");
        input.setAttribute("data-original", values[value]);
        input.setAttribute("class", "form-control input-sm");
        input.setAttribute("type", "text");
        input.setAttribute("name", value);
        input.setAttribute("id", value);
        input.setAttribute("value", values[value]);
        td.appendChild(input);
        return td;
      }

      function setTableEditable(values, row) {
        for (let value in values) {
          let td = generateEditableRow(values, value);
          row.lastElementChild.insertAdjacentElement("beforebegin", td);
        }
      }

      function hideElement(elementArray) {
        for (let element of elementArray) {
          element.style.display = "none";
        }
      }

      function showElement(elementArray) {
        for (let element of elementArray) {
          element.style.display = "block";
        }
      }
      
      // For Add
      function add(event) {
        let btn = event.target.parentElement;
        let row = event.target.dataset.row;
        values = {};
        let mainRow = document.getElementById(row);
        if (btn.classList.contains("bConfirm")) {
          let data = document.getElementById(row).querySelectorAll("input");
          let editButton = document.getElementById(`bEdit${row}`);
          let deleteButton = document.getElementById(`bDelete${row}`);
          let confirmButton = document.getElementById(`bConfirm${row}`);
          let cancelButton = document.getElementById(`bCancel${row}`);

          for (let i = 0; i < data.length; i++) {
            let key = data[i].getAttribute("name");
            let value = data[i].value;
            values[key] = value;
          }

        // call api
          
        //   hideElement([confirmButton, cancelButton]);
        //   showElement([editButton, deleteButton]);

        // if response is ok
          
          data.forEach(d => {
              d.parentElement.remove();
          });

          // this would be id of the response data
          let serial = Math.ceil(((Math.random() * 100) + 10) * Math.random() + 10);
          // mainRow.firstChild.innerHTML = serial;
          // setDefaultTable(values, mainRow);
          let newData = {"id": serial, ...values};
          let totalNumberofRows = document.getElementsByTagName('tr');
          let newRow = buildRow(newData, totalNumberofRows.length - 1);
          mainRow.innerHTML = '';
          mainRow.innerHTML = newRow;
          mainRow.id = newData.id;

        // mainData.push({"id": Math.random(), ...values});
        // setData();

        }
        if (btn.classList.contains("bCancel")) {
          mainRow.remove();
        }
      }

      // For Edit
      function edit(event) {
        let btn = event.target.parentElement;
        let row = event.target.dataset.row;
        let values = {};
        let mainRow = document.getElementById(row);
        if (btn.classList.contains("bEdit")) {
          let data = document.getElementById(row).querySelectorAll("td");
          let editButton = document.getElementById(`bEdit${row}`);
          let deleteButton = document.getElementById(`bDelete${row}`);
          let confirmButton = document.getElementById(`bConfirm${row}`);
          let cancelButton = document.getElementById(`bCancel${row}`);
          hideElement([editButton, deleteButton]);
          showElement([confirmButton, cancelButton]);
          for (let i = 0; i < data.length - 1; i++) {
            let key = data[i].getAttribute("col_name");
            values[key] = `${data[i].innerText}`;
            data[i].remove();
          }
          setTableEditable(values, mainRow);
        }
      }

      // For Cancel
      function cancel(event) {
        let btn = event.target.parentElement;
        let row = event.target.dataset.row;
        let values = {};
        let mainRow = document.getElementById(row);
        if (btn.classList.contains("bCancel")) {
          let data = document.getElementById(row).querySelectorAll("input");
          let editButton = document.getElementById(`bEdit${row}`);
          let deleteButton = document.getElementById(`bDelete${row}`);
          let confirmButton = document.getElementById(`bConfirm${row}`);
          let cancelButton = document.getElementById(`bCancel${row}`);
          hideElement([confirmButton, cancelButton]);
          showElement([editButton, deleteButton]);

          for (let i = 0; i < data.length; i++) {
            let key = data[i].getAttribute("name");
            let value = data[i].dataset.original;
            values[key] = value;
            data[i].parentElement.remove();
          }

          setDefaultTable(values, mainRow);
        }
      }

      // For Confirm
      function confirm(event) {
        let btn = event.target.parentElement;
        let row = event.target.dataset.row;
        let values = {};
        let mainRow = document.getElementById(row);
        if (btn.classList.contains("bConfirm")) {
          let data = document.getElementById(row).querySelectorAll("input");
          let editButton = document.getElementById(`bEdit${row}`);
          let deleteButton = document.getElementById(`bDelete${row}`);
          let confirmButton = document.getElementById(`bConfirm${row}`);
          let cancelButton = document.getElementById(`bCancel${row}`);

          for (let i = 0; i < data.length; i++) {
            let key = data[i].getAttribute("name");
            let value = data[i].value;
            values[key] = value;
            data[i].parentElement.remove();
          }
          // call api
          console.log("data id is", row);
          hideElement([confirmButton, cancelButton]);
          showElement([editButton, deleteButton]);
          setDefaultTable(values, mainRow);
        }
      }

      function setChoice(event, choice) {
        if(choice) {
          deleteRow(event.target.dataset.row);
        }
      }

      // For Delete
      function deleteRow(row) {
        let mainRow = document.getElementById(row);
        mainRow.remove();
      }
